# Python Selenium Test Framework

> This is a web test framework is its only purpose is to test the technologies implemented in this project.

The framework is based in python, selenium, pytest and implements allure framework for reports out.

	This is only a test and should not be taken to use in production.

## Pre-requisites

- Install Python version 3 or above.
- Install python selenium through PIP dependency manager.
- Install Pytest thought PIP dependency manager.
- Install Allure framework.
- Instalal GIT.

## How to execute

To start run the following command line the file:

	run.bat

To see allure reports you need to have Allure framework installed, if you already have it, the commnad will display in browser a HTML based report.

## Contact

	@edanca512