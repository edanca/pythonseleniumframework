import logging
import setup

def logger_object():
    logger = logging.getLogger(__name__)
    logger.setLevel(setup.LOGGING_LEVEL)
    logger.propagate = False
    # log_config('test.log', logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s:%(levelno)s:%(levelname)s:%(message)s')

    file_handler = logging.FileHandler('../Logs/tests.log')
    file_handler.setFormatter(formatter)

    logger.addHandler(file_handler)

    return logger
    
def error(msg, result=None, expected=None):
    logger = logger_object()
    if len(logger.handlers) > 1:
        logger.handlers.pop()

    if result is None and expected is None:
        logger.error(msg)
    else:
        message = f'{msg} || RESULT: {result} || EXPECTED: {expected}'
        logger.error(message)

def info(msg):
    logger = logger_object()
    if len(logger.handlers) > 1:
        logger.handlers.pop()
    logger.info(msg)

def start_test_case(title):
    msg = '================================================================='
    logger = logger_object()
    if len(logger.handlers) > 1:
        logger.handlers.pop()
    logger.info(msg)
    logger.info(f'STARTING {title}')

def start_test_plan():
    msg = '\\n\\r'
