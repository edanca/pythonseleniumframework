import time
import logging

__author__ = "Eduardo Cárdenas"


def _validate_html_title(driver, title):
    # assert title in driver.title
    if not title in driver.title:
        logging.error(get_time(), '"{}" was not found on Page title.'.format(title))
        logging.error(AssertionError())
        #raise AssertionError()
    else:
        return True


def _validate_class(elem, class_name):
    is_flag_icon = class_name in elem.get_attribute("class")

    if is_flag_icon:
        assert True

    return False


def _validate_tag_name(elem, tag_name):
    result = False
    try:
        if elem.tag_name == tag_name:
            result = True
        assert result == True

    except Exception as e:
        raise e

    return result


def _validate_email_value(elem, email):
    result = False
    try:
        if elem.text == email:
            result = True
        assert result == True

    except Exception as e:
        raise e

    return result


def _validate_text(elem, text):
    result = False
    try:
        if text in elem.text:
            result = True
        assert text in elem.text
    except Exception as e:
        result = elem.text
        #raise e

    return result


def get_time():
    return time.time()