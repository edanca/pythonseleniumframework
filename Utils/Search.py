from Config import setUp
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoSuchFrameException
from selenium.common.exceptions import StaleElementReferenceException
from selenium.common.exceptions import WebDriverException
from selenium.common.exceptions import NoAlertPresentException
import pytest

__author__ = "Eduardo Cárdenas"

def _find_element(driver, *args):
    # [driver, ByCSS, selector] = args
    try:
        return driver.find_element(*args)
    except NoSuchElementException as e:
        print("========== NO ELEMENT WERE FOUND ==========")
        raise e
    except WebDriverException as e:
        raise e
    except Exception as e:
        raise e


def _find_element_by_xpath(driver, xpath):
    try:
        return driver.find_element_by_xpath(xpath)
    except NoSuchElementException as e:
        print("========== NO ELEMENT WERE FOUND ==========")
        raise e
    except WebDriverException as e:
        raise e
    except Exception as e:
        raise e


def _find_elements_by_xpath(driver, xpath):
    try:
        return driver.find_elements_by_xpath(xpath)
    except NoSuchElementException as e:
        print("========== NO ELEMENT WERE FOUND ==========")
        raise e
    except WebDriverException as e:
        raise e
    except Exception as e:
        raise e


def _find_element_by_id(driver, id):
    try:
        return driver.find_element_by_id(id)
    except NoSuchElementException as e:
        print("========== NO ELEMENT WERE FOUND ==========")
        raise e
    except WebDriverException as e:
        raise e
    except Exception as e:
        raise e


def _find_element_by_class_name(driver, class_name):
    try:
        return driver.find_element_by_class_name(class_name)
    except NoSuchElementException as e:
        print("========== NO ELEMENT WERE FOUND ==========")
        raise e
    except WebDriverException as e:
        raise e
    except Exception as e:
        raise e


def _find_element_by_name(driver, name):
    try:
        return driver.find_element_by_name(name)
    except NoSuchElementException as e:
        print("========== NO ELEMENT WERE FOUND ==========")
        raise e
    except WebDriverException as e:
        raise e
    except Exception as e:
        raise e


def _find_elements_by_name(driver, name):
    try:
        return driver.find_elements_by_name(name)
    except NoSuchElementException as e:
        print("========== NO ELEMENT WERE FOUND ==========")
        raise e
    except WebDriverException as e:
        raise e
    except Exception as e:
        raise e


def _find_element_by_tag_name(driver, tag_name):
    try:
        return driver.find_element_by_tag_name(tag_name)
    except NoSuchElementException as e:
        print("========== NO ELEMENT WERE FOUND ==========")
        raise e
    except WebDriverException as e:
        raise e
    except Exception as e:
        raise e


def _find_elements_by_tag_name(driver, tag_name):
    try:
        return driver.find_elements_by_tag_name(tag_name)
    except NoSuchElementException as e:
        print("========== NO ELEMENT WERE FOUND ==========")
        raise e
    except WebDriverException as e:
        raise e
    except Exception as e:
        raise e


def _find_element_by_link_text(driver, link_text):
    try:
        return driver.find_element_by_link_text(link_text)
    except NoSuchElementException as e:
        print("========== NO ELEMENT WERE FOUND ==========")
        raise e
    except WebDriverException as e:
        raise e
    except Exception as e:
        raise e


def _find_elements_by_link_text(driver, link_text):
    try:
        return driver.find_elements_by_link_text(link_text)
    except NoSuchElementException as e:
        print("========== NO ELEMENT WERE FOUND ==========")
        raise e
    except WebDriverException as e:
        raise e
    except Exception as e:
        raise e


def _find_element_by_partial_link_text(driver, link_text):
    try:
        return driver.find_element_by_partial_link_text(link_text)
    except NoSuchElementException as e:
        print("========== NO ELEMENT WERE FOUND ==========")
        raise e
    except WebDriverException as e:
        raise e
    except Exception as e:
        raise e


def _find_elemens_by_partial_link_text(driver, link_text):
    try:
        return driver.find_elements_by_partial_link_text(link_text)
    except NoSuchElementException as e:
        print("========== NO ELEMENT WERE FOUND ==========")
        raise e
    except WebDriverException as e:
        raise e
    except Exception as e:
        raise e


def _find_element_by_css_selector(driver, css_selector):
    try:
        return driver.find_element_by_css_selector(css_selector)
    except NoSuchElementException as e:
        print("========== NO ELEMENT WERE FOUND ==========")
        raise e
    except WebDriverException as e:
        raise e
    except Exception as e:
        raise e


def _find_elements_by_css_selector(driver, css_selector):
    try:
        return driver.find_elements_by_css_selector(css_selector)
    except NoSuchElementException as e:
        print("========== NO ELEMENT WERE FOUND ==========")
        raise e
    except WebDriverException as e:
        raise e
    except Exception as e:
        raise e
