from Config import Chrome
from Config import configFile
from Utils import log
from selenium.webdriver.support.ui import WebDriverWait
import time
import logging
import pytest

__aurhor__ = "Eduardo Cárdenas"

class Test_Base():

    def mySetUp(self, url=None):
        if url is None:
            self.base_URL = configFile.baseURL
        else:
            self.base_URL = url

        self.driver = Chrome.getDriver(self.base_URL)
        self.wait = WebDriverWait(self.driver, 5, poll_frequency=1.0)

        return self.driver

    def out_test(self, driver, times=1):
        # Operations after complete a test
        for i in range(1):
            driver.back()
        time.sleep(1)
