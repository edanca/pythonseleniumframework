from Tests.test_base import Test_Base
from Utils import Search, Common, log
from Pages.root import Root
import pytest
import time
import numpy as np

class Test_Root(Test_Base):

    @pytest.fixture(scope="module")
    def driver(self):
        url = "https://www.wikipedia.org/"
        try:
          driver = self.mySetUp(url)
          yield driver
        finally:
          driver.close()

    def test_basic_search(self, driver):
        test_name = 'test_basic_search'
        log.start_test_case(test_name)

        text_to_search = "wikipedia"
        page = Root()
        page.set_search(driver, text_to_search)

        #title_es = "la enciclopedia libre"
        #result = Common._validate_html_title(driver, title_es)

        expected_title = 'Wikipedia'
        title_id = 'firstHeading'
        elem = Search._find_element_by_id(driver, title_id)
        result = Common._validate_text(elem, expected_title)

        if result is not True:
            text_log = f'Error on test {test_name}'
            log.error(text_log, result, expected_title)
            assert False

        log.info(f'Test {test_name} PASS')

        self.out_test(driver)

    def test_go_to_portada_es(self, driver):
        test_name = "test_go_to_portada_es"
        log.start_test_case(test_name)

        page = Root()
        page.go_to_page_portada_es(driver)
        time.sleep(3)

        title_es = "la enciclopedia libre"
        Common._validate_html_title(driver, title_es)

        log.info(f'Test {test_name} PASS')

        self.out_test(driver)

    def test_check_languages_titles(self, driver):
        test_name = "test_check_languages_titles"
        log.start_test_case(test_name)

        page = Root()
        arr_lang = np.array([
            ["#js-link-box-es > strong", "Español"],
            ["#js-link-box-en > strong", "English"],
            ["#js-link-box-fr > strong", "Français"],
            ["#js-link-box-pl > strong", "Polski"],
            ["#js-link-box-ja > strong", "日本語"],
            ["#js-link-box-de > strong", "Deutsch"],
            ["#js-link-box-ru > strong", "Русский"],
            ["#js-link-box-it > strong", "Italiano"],
            ["#js-link-box-zh > strong", "中文"],
            ["#js-link-box-pt > strong", "Português"],
        ])
        time.sleep(3)
        for i in np.arange(len(arr_lang)):
            elem = page.get_elem_by_css(driver, arr_lang[i][0])
            result = Common._validate_text(elem, arr_lang[i][1])

            if result is not True:
                text_log = f"Validate languages failed. {arr_lang[i][1]} NOT FOUND."
                self.log(text_log)

        log.info(f'Test {test_name} PASS')

        self.out_test(driver)
