from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from Utils import Common, Search
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoSuchFrameException
from selenium.common.exceptions import StaleElementReferenceException
from selenium.common.exceptions import WebDriverException
from selenium.common.exceptions import NoAlertPresentException

class Root():

	def set_search(self, driver, search_input):
		self.search_bar = 'searchInput'
		elem = Search._find_element_by_id(driver, self.search_bar)

		if elem is None:
			self.search_bar_selector = '#searchInput'
			elem = Search._find_element(driver, By.CSS_SELECTOR, self.search_bar_selector)

		if elem is None:
			raise NoSuchElementException

		elem.clear()
		elem.send_keys(search_input)
		elem.send_keys(Keys.RETURN)


	def sidebar_elem(self, driver):
		pass


	def go_to_page_portada_es(self, driver):
		id = "js-link-box-es"
		elem = Search._find_element_by_id(driver, id)
		elem.click()


	def get_links_in_page(self, driver):
		elems_link = Search._find_elements_by_xpath('.//a')
		links = []

		for a in elems_link:
			links.append(a.get_sttribute('href'))

		return links


	def get_elem_by_css(self, driver, css_selector):
		elem = Search._find_element_by_css_selector(driver, css_selector)
		return elem
