del results\*.txt
del results\*.json

python -m pytest test.py -v --alluredir ./results

allure serve ./results/
