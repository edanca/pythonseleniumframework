set testCase=%1
del results\*.txt
del results\*.json

python -m pytest Tests\%testCase% --alluredir ./results

allure serve ./results/
